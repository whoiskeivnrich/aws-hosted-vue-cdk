### ADR1

Q: What method will we use to specify account and region for the stack?
A: Follow [Developer Guide Recommendation](https://docs.aws.amazon.com/cdk/v2/guide/environments.html):
> For production stacks, we recommend that you explicitly specify the environment for each stack in your app using the env property
R: Follows best practices as demonstrated in developer guide

### ADR2
Q: Where do we pull account and region from?
A: process.env.CDK_DEFAULT_ACCOUNT && process.env.CDK_DEFAULT_REGION
R: Leverage AWS CLI defaults for proof of concept


### ADR3
Q: Should the ChatOps infra be in it's own stack?
A: Yes
R: Infrastructure deployment will failing if trying to redeploy the same Workspace/Channel configuration