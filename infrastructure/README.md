# Welcome to your CDK TypeScript project!

This is a blank project for TypeScript development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

## Setting up the stack

1. Install the [AWS CLI](https://aws.amazon.com/cli/)
1. [Configure the cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-quickstart.html): `aws configure`
1. Deploy the configuration stack: `cdk deploy BookingConfig`
2. Set the [Channel ID](https://us-west-2.console.aws.amazon.com/systems-manager/parameters/%2Fbookings%2Fslack%2Fchannel-id/description?region=us-west-2) and [Workspace ID](https://us-west-2.console.aws.amazon.com/systems-manager/parameters/%2Fbookings%2Fslack%2Fworkspace-id/description?region=us-west-2) for Slack Notifications
3. Deploy the application stack: `cdk deploy BookingApp`
## Architectural Decision Record
See [ADR.md](./ADR.md)