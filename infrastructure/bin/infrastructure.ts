#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import BookingAppStack from '../lib/stacks/booking-stack';
import { Tags } from 'aws-cdk-lib';

// ADR1
const ProductionEnv = {
    // ADR2
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
};

const bookingApp = new cdk.App();

const bookingSaga = new BookingAppStack(bookingApp, 'BookingApp', {
    env: ProductionEnv
});

Tags.of(bookingApp).add("Source", "Bitbucket: aws-hosted-vue-cdk");
Tags.of(bookingApp).add("Product", "Booking Saga Application");