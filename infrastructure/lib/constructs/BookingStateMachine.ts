import { aws_dynamodb, Duration } from 'aws-cdk-lib';
import { GraphWidget, IWidget, Metric, SingleValueWidget, Statistic, TextWidget } from 'aws-cdk-lib/aws-cloudwatch';
import { MonitoredNotificationTopic } from './monitored-notification-topic';
import { DynamoAttributeValue, DynamoDeleteItem, DynamoPutItem, LambdaInvoke, SnsPublish } from 'aws-cdk-lib/aws-stepfunctions-tasks';
import { Chain, Fail, IntegrationPattern, JsonPath, StateMachine, Succeed, TaskInput } from 'aws-cdk-lib/aws-stepfunctions';
import { Code, Function, Runtime } from 'aws-cdk-lib/aws-lambda';
import { MonitoredDynamoTable } from './monitored-dynamo-table';
import { Construct } from 'constructs';
import { Effect, Group, PolicyStatement, Role } from 'aws-cdk-lib/aws-iam';


export class BookingStateMachine extends Construct {
    public stateMachine: StateMachine;
    public dashboardWidgets: IWidget[] = [];

    constructor(scope: Construct, id: string) {
        super(scope, id);

        const notifications = new MonitoredNotificationTopic(this, 'Notifications');
        this.dashboardWidgets.push(...notifications.dashboardWidgets);

        const flightTable = new MonitoredDynamoTable(this, 'Flights', {
            partitionKey: { name: 'pk', type: aws_dynamodb.AttributeType.STRING },
            sortKey: { name: 'sk', type: aws_dynamodb.AttributeType.STRING }
        });
        this.dashboardWidgets.push(...flightTable.dashboardWidgets);


        const reservationFailed = new Fail(this, 'Reservation Failed', { error: 'Job Failed' });
        const reservationSucceeded = new Succeed(this, 'Reservation Succeed');

        const notificationFailure = new SnsPublish(this, 'SendingSMSFailure', {
            topic: notifications.topic,
            integrationPattern: IntegrationPattern.REQUEST_RESPONSE,
            message: TaskInput.fromText('Your travel reservation failed')
        });

        const notificationSuccess = new SnsPublish(this, 'SendingSMSSuccess', {
            topic: notifications.topic,
            integrationPattern: IntegrationPattern.REQUEST_RESPONSE,
            message: TaskInput.fromText('Your travel reservation was successfully booked')
        });

        const deleteFlightReservation = new DynamoDeleteItem(this, 'Delete Flight Reservation', {
            resultPath: '$.CancelFlightReservationResult',
            table: flightTable.table,
            key: { 'pk': DynamoAttributeValue.fromString(JsonPath.stringAt('$.trip_id')) },
        }).addRetry({ maxAttempts: 3 })
            .next(notificationFailure)
            .next(reservationFailed);

        const persistFlightReservation = new DynamoPutItem(this, 'Persist Flight Reservation', {
            resultPath: '$.ReserveFlightResult',
            table: flightTable.table,
            item: {
                'pk': DynamoAttributeValue.fromString(JsonPath.stringAt('$.trip_id')),
                'sk': DynamoAttributeValue.fromString(JsonPath.stringAt('$.BuildReserveFlightResult.Payload.flightReservationId')),
                'trip_id': DynamoAttributeValue.fromString(JsonPath.stringAt('$.trip_id')),
                'depart_city': DynamoAttributeValue.fromString(JsonPath.stringAt('$.depart_city')),
                'depart_time': DynamoAttributeValue.fromString(JsonPath.stringAt('$.depart_time')),
                'arrive_city': DynamoAttributeValue.fromString(JsonPath.stringAt('$.arrive_city')),
                'arrive_time': DynamoAttributeValue.fromString(JsonPath.stringAt('$.arrive_time')),
                'rental': DynamoAttributeValue.fromString(JsonPath.stringAt('$.rental')),
                'rental_from': DynamoAttributeValue.fromString(JsonPath.stringAt('$.rental_from')),
                'rental_to': DynamoAttributeValue.fromString(JsonPath.stringAt('$.rental_to')),
                'run_type': DynamoAttributeValue.fromString(JsonPath.stringAt('$.run_type')),
            },
        }).addCatch(deleteFlightReservation, {
            resultPath: '$.ReserveFlightError'
        });

        const buildFlightReservation = new LambdaInvoke(this, 'Build Flight Reservation', {
            resultPath: '$.BuildReserveFlightResult',
            lambdaFunction: this.createLambda(this, 'reserveFlightLambdaHandler', 'flights/reserveFlight.handler')
        }).addCatch(deleteFlightReservation);


        const stepFunctionDefinition = Chain.start(buildFlightReservation)
            .next(persistFlightReservation)
            .next(notificationSuccess)
            .next(reservationSucceeded);

        const saga = new StateMachine(this, 'StateMachine', { definition: stepFunctionDefinition });

        const sagaLambda = new Function(this, 'SagaLambdaHandler', {
            runtime: Runtime.NODEJS_12_X,
            code: Code.fromAsset('../src/lambdas'),
            handler: 'bookingSaga.handler',
            environment: { statemachine_arn: saga.stateMachineArn },
        });
        const executionGroup = new Group(this, 'BookingExecutors');
        sagaLambda.grantInvoke(executionGroup);
        const sagaLambdaWidgets = this.defineLambdaDashboardWidgets(sagaLambda);
        this.dashboardWidgets.push(...sagaLambdaWidgets)

        saga.grantStartExecution(sagaLambda);
    }

    createLambda(scope: Construct, id: string, handler: string) {
        let fn = new Function(scope, id, {
            runtime: Runtime.NODEJS_12_X,
            code: Code.fromAsset('../src/lambdas'),
            handler
        });

        return fn;
    }

    defineLambdaDashboardWidgets(fn: Function) {
        let metrics = this.defineLambdaMetrics(fn);
        let widgets = new Array<IWidget>();

        widgets.push(new TextWidget({
            markdown: `Lambda \| ${fn.functionName}`,
            height: 1, width: 24,
        }));

        widgets.push(new SingleValueWidget({
            height: 4, width: 24,
            setPeriodToTimeRange: true,
            metrics: metrics,
            title: 'Metrics'
        }));

        for (const metric of metrics) {
            widgets.push(new GraphWidget({
                height: 4, width: 8,
                setPeriodToTimeRange: true,
                left: [metric]
            }))
        }

        return widgets;
    }

    defineLambdaMetrics(fn: Function) {
        const namespace = 'AWS/Lambda';

        let metrics = new Array<Metric>();
        metrics.push(new Metric({
            namespace: namespace,
            metricName: 'Invocations',
            label: 'Total Invocations',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'FunctionName': fn.functionName
            }
        }));

        metrics.push(new Metric({
            namespace: namespace,
            metricName: 'Errors',
            label: 'Total Errors',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'FunctionName': fn.functionName
            }
        }));

        metrics.push(new Metric({
            namespace: namespace,
            metricName: 'Duration',
            label: 'Avg Duration',
            statistic: Statistic.AVERAGE,
            period: Duration.minutes(5),
            dimensionsMap: {
                'FunctionName': fn.functionName
            }
        }));

        return metrics;
    }
}
