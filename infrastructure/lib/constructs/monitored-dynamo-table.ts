import { Duration, RemovalPolicy } from "aws-cdk-lib";
import { GraphWidget, IWidget, Metric, SingleValueWidget, Statistic, TextWidget } from "aws-cdk-lib/aws-cloudwatch";
import { Attribute, Table } from "aws-cdk-lib/aws-dynamodb";
import { Construct } from "constructs";

export interface MonitoredDynamoTableProps {
    partitionKey: Attribute;
    sortKey?: Attribute;
}

const NAMESPACE = 'AWS/DynamoDB';

export class MonitoredDynamoTable extends Construct {
    public readonly table: Table;
    public readonly dashboardWidgets: IWidget[] = [];

    constructor(scope: Construct, id: string, props: MonitoredDynamoTableProps) {
        super(scope, id);

        this.table = new Table(this, 'Table', {
            partitionKey: props.partitionKey,
            sortKey: props.sortKey,

            // don't do this for prod
            removalPolicy: RemovalPolicy.DESTROY
        })

        const consoleUrl = `https://${this.table.env.region}.console.aws.amazon.com/dynamodbv2/home?region=${this.table.env.region}#table?initialTagKey=&name=${this.table.tableName}&tab=overview`
        this.dashboardWidgets.push(new TextWidget({
            markdown: `[DynamoDB Table \| ${this.table.tableName}](${consoleUrl})`,
            height: 1, width: 24
        }));

        const systemErrorMetrics = this.getSystemErrorMetrics()
        this.dashboardWidgets.push(new SingleValueWidget({
            height: 4, width: 12,
            setPeriodToTimeRange: true,
            metrics: systemErrorMetrics
        }));

        this.dashboardWidgets.push(new GraphWidget({
            height: 4, width: 12,
            setPeriodToTimeRange: true,
            left: systemErrorMetrics
        }));

    }

    getSystemErrorMetrics() {
        let metrics = new Array<Metric>();

        metrics.push(new Metric({
            namespace: NAMESPACE,
            metricName: 'SystemErrors',
            label: 'GetItem',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'TableName': this.table.tableName,
                'Operation': 'GetItem',
            }
        }));

        metrics.push(new Metric({
            namespace: NAMESPACE,
            metricName: 'SystemErrors',
            label: 'Scan',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'TableName': this.table.tableName,
                'Operation': 'Scan',
            }
        }));

        metrics.push(new Metric({
            namespace: NAMESPACE,
            metricName: 'SystemErrors',
            label: 'Query',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'TableName': this.table.tableName,
                'Operation': 'Query',
            }
        }));

        metrics.push(new Metric({
            namespace: NAMESPACE,
            metricName: 'SystemErrors',
            label: 'BatchGetItem',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            dimensionsMap: {
                'TableName': this.table.tableName,
                'Operation': 'BatchGetItem',
            }
        }));

        return metrics;
    }
}
