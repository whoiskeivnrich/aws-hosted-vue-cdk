import { Duration, Stack } from 'aws-cdk-lib';
import { Color, GraphWidget, IWidget, Metric, SingleValueWidget, Statistic, TextWidget } from 'aws-cdk-lib/aws-cloudwatch';
import { Topic } from 'aws-cdk-lib/aws-sns';
import { Construct } from 'constructs';


export class MonitoredNotificationTopic extends Construct {
    public readonly topic: Topic;
    public readonly dashboardWidgets: IWidget[] = [];

    constructor(scope: Construct, id: string) {
        super(scope, id);

        this.topic = new Topic(this, 'Topic');

        const snsMetricNamespace = 'AWS/SNS';
        const snsMetricDimensions = { 'TopicName': this.topic.topicName };


        let consoleUrl = `${this.topic.env.region}.console.aws.amazon.com/sns/v3/home?region=${this.topic.env.region}#/topic/${this.topic.topicArn.replace(':', '%3A')}`;
        const description = new TextWidget({
            markdown: `[SNS Topic \| ${this.topic.topicName}](${consoleUrl})`,
            height: 1, width: 24
        });

        const topicNotificationsTotalMetric = new Metric({
            metricName: 'Total Notifications',
            label: 'Delivered Notifications',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            color: Color.BLUE,
            namespace: snsMetricNamespace,
            dimensionsMap: snsMetricDimensions,
        });

        const topicAvgPublishSizeMetric = new Metric({
            metricName: 'PublishSize',
            label: 'Average Publish Size',
            statistic: Statistic.AVERAGE,
            period: Duration.minutes(5),
            color: Color.BROWN,
            namespace: snsMetricNamespace,
            dimensionsMap: snsMetricDimensions
        });

        const topicFailedNotificationsTotalMetric = new Metric({
            metricName: 'NumberOfNotificationsFailed',
            label: 'Failed Notifications',
            statistic: Statistic.SUM,
            period: Duration.minutes(5),
            color: Color.RED,
            namespace: snsMetricNamespace,
            dimensionsMap: snsMetricDimensions
        });

        const valueWidget = new SingleValueWidget({
            height: 4, width: 24,
            setPeriodToTimeRange: true,
            metrics: [topicNotificationsTotalMetric, topicFailedNotificationsTotalMetric, topicAvgPublishSizeMetric]
        });

        const topicTotalNotificationsGraph = new GraphWidget({
            height: 4, width: 8,
            left: [topicNotificationsTotalMetric]
        });

        const topicFailedNotificationsGraph = new GraphWidget({
            height: 4, width: 8,
            left: [topicFailedNotificationsTotalMetric]
        });

        const topicPublishSizeGraph = new GraphWidget({
            height: 4, width: 8,
            left: [topicAvgPublishSizeMetric]
        });

        this.dashboardWidgets.push(description, valueWidget, topicTotalNotificationsGraph, topicFailedNotificationsGraph, topicPublishSizeGraph);
    }
}
