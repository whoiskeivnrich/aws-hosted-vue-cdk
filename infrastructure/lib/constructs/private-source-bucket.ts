import * as cdk from 'aws-cdk-lib';
import * as path from 'path';
import { Bucket, BucketAccessControl } from 'aws-cdk-lib/aws-s3';
import { BucketDeployment } from 'aws-cdk-lib/aws-s3-deployment';
import { Construct } from 'constructs';

export interface StaticSiteDeploymentProps {
    sourceDir: string;
}

export class StaticSiteDeployment extends Construct {
    public readonly bucket: Bucket;
    private _deployProps: StaticSiteDeploymentProps;

    constructor(scope: Construct, id: string, props: StaticSiteDeploymentProps) {
        super(scope,id);
        this._deployProps = props;

        // create bucket to host code without exposing it via S3
        this.bucket = new Bucket(this, 'host-bucket', {
            accessControl: BucketAccessControl.PRIVATE,
        });
        
        // create deployment to bucket
        new BucketDeployment(this, 'deployment', {
            destinationBucket: this.bucket,
            sources: [
                cdk.aws_s3_deployment.Source.asset(path.resolve(__dirname, this._deployProps.sourceDir))
            ]
        });
    }
}