import { App, Stack, StackProps } from 'aws-cdk-lib';
import { Dashboard } from 'aws-cdk-lib/aws-cloudwatch';
import { BookingStateMachine } from '../constructs/BookingStateMachine';

export interface BookingProps extends StackProps {

}

export default class BookingAppStack extends Stack {

    constructor(scope: App, id: string, props: BookingProps) {
        super(scope, id, props);

        const oneWeekRelativeRangeStart = '-P1W';
        const dashboard = new Dashboard(this, 'BookingDashboard', {
            start: oneWeekRelativeRangeStart
        });

        const stateMachine = new BookingStateMachine(this, 'BookingStateMachine');
        dashboard.addWidgets(...stateMachine.dashboardWidgets);
    }
}