import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import { BucketAccessControl } from 'aws-cdk-lib/aws-s3';
import { StaticApplicationStack } from '../lib/application-stack';

describe('StaticApplicationStack', () => {

    let template: Template;

    beforeAll(() => {
        const app = new cdk.App();
        const stack = new StaticApplicationStack(app, 'MyTestStack');
        template = Template.fromStack(stack);
    });
    
    let x = 

    test('should create S3 Bucket', () => {    
        template.hasResourceProperties('AWS::S3::Bucket', {
            AccessControl: BucketAccessControl.PRIVATE
        });
    })

    test('should create CloudFront Distribution', () => {
        template.hasResourceProperties('AWS::CloudFront::Distribution', {
            DistributionConfig: {
                DefaultRootObject: "index.html",
                HttpVersion: "http2",
                DefaultCacheBehavior: {
                    ViewerProtocolPolicy: "allow-all",
                }
            }
        })
    })
});
