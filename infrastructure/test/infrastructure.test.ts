import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import { BucketAccessControl } from 'aws-cdk-lib/aws-s3';
import StaticApplicationStack from '../lib/application-stack';

describe('StaticApplicationStack', () => {
    test('should create bucket', () => {
        const app = new cdk.App();
        // WHEN
        const stack = new StaticApplicationStack(app, 'MyTestStack');
        // THEN
        const template = Template.fromStack(stack);
    
        template.hasResourceProperties('AWS::S3::Bucket', {
            AccessControl: BucketAccessControl.PRIVATE
        });
    })
});
