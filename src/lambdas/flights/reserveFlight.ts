import { hashString } from '../helpers';

exports.handler = async function (event: any) {
    console.log('request:', JSON.stringify(event, undefined, 2));

    let flightReservationId = hashString(`${event.depart_city}${event.arrive_city}`);
    console.log('flightReservationId:', flightReservationId);

    if (event.run_type === 'failFlightReservation') {
        throw new Error('Failed to book the flights');
    }

    return {
        status: 'ok',
        flightReservationId
    }
}