export function hashString(input: string) {
    let result: any;

    for (let i = 0; i < input.length; i++) {
        result = Math.imul(31, result) + input.charCodeAt(i) | 0;
    }

    return `${Math.abs(result)}`
}